package com.freecharge.PennyDrop.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.freecharge.PennyDrop.Service.AccountVerification;
import com.freecharge.PennyDrop.model.AcctVerifyRequest;
import com.freecharge.PennyDrop.utils.Constants;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

@RestController
public class Controller {

    @Resource
    AccountVerification accountVerification;


    @ApiOperation(value = "Account Verification Request", notes = "This operation is used to check account verify or penny drop call")
    @RequestMapping(value = Constants.ACC_VERI_URL, method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<String> diagnosePing(@Valid @RequestBody AcctVerifyRequest acctVerifyRequest) throws JsonProcessingException {
        return accountVerification.verify(acctVerifyRequest);
    }
}
