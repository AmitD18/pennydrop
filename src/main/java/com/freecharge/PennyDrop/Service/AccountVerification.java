package com.freecharge.PennyDrop.Service;

import com.freecharge.PennyDrop.model.AcctVerifyRequest;
import org.springframework.http.ResponseEntity;

public interface AccountVerification {

    ResponseEntity<String> verify(AcctVerifyRequest acctVerifyRequest);
}
