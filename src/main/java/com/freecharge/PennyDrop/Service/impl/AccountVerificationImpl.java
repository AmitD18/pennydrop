package com.freecharge.PennyDrop.Service.impl;

import com.freecharge.PennyDrop.Service.AccountVerification;
import com.freecharge.PennyDrop.facade.Facade;
import com.freecharge.PennyDrop.model.AcctVerifyRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AccountVerificationImpl implements AccountVerification {
    @Autowired
    Facade facade;
    @Override
    public ResponseEntity<String> verify(AcctVerifyRequest acctVerifyRequest) {
        //return new ResponseEntity<>("1234", HttpStatus.OK);
        return new ResponseEntity<>(facade.verifyPennyDrop(acctVerifyRequest), HttpStatus.OK);
    }
}
