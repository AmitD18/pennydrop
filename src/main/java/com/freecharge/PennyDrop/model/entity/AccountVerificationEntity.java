package com.freecharge.PennyDrop.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;

@Entity
@Table(name = "account_verification_status")
public class AccountVerificationEntity {
    @Id
    private Integer id;
    private String userId;
    private String status;
    private String metatData;
    private Date createdOn;
    private Date updatedOn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMetatData() {
        return metatData;
    }

    public void setMetatData(String metatData) {
        this.metatData = metatData;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "AccountVerificationEntity{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", status='" + status + '\'' +
                ", metatData='" + metatData + '\'' +
                ", createdOn=" + createdOn +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
