package com.freecharge.PennyDrop.model;


import jdk.nashorn.internal.objects.annotations.Constructor;

public class APIResponse {
    private String UserId;

    public APIResponse(String userId) {
        UserId = userId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    @Override
    public String toString() {
        return "APIResponse{" +
                "UserId='" + UserId + '\'' +
                '}';
    }
}
