package com.freecharge.PennyDrop.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;

public class AcctVerifyRequest {
    @NotNull
    private String accountNumber;
    @NotNull
    private String ifscCode;
    @NotNull
    private String name;
    private String userId;
    @NotNull
    private String pan;
    @NotNull
    private String mobile;
    @NotNull
    private String refrenceId;
    @NotNull
    private Integer usecaseId;
    @NotNull
    private Integer remittorId;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRefrenceId() {
        return refrenceId;
    }

    public void setRefrenceId(String refrenceId) {
        this.refrenceId = refrenceId;
    }

    public Integer getUsecaseId() {
        return usecaseId;
    }

    public void setUsecaseId(Integer usecaseId) {
        this.usecaseId = usecaseId;
    }

    public Integer getRemittorId() {
        return remittorId;
    }

    public void setRemittorId(Integer remittorId) {
        this.remittorId = remittorId;
    }

    @Override
    public String toString() {
        return "RequestBodyType{" +
                "accountNumber='" + accountNumber + '\'' +
                ", ifscCode='" + ifscCode + '\'' +
                ", name='" + name + '\'' +
                ", userId='" + userId + '\'' +
                ", pan='" + pan + '\'' +
                ", mobile='" + mobile + '\'' +
                ", refrenceId='" + refrenceId + '\'' +
                ", usecaseId=" + usecaseId +
                ", remittorId=" + remittorId +
                '}';
    }
}
