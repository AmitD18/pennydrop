package com.freecharge.PennyDrop.utils;

public class Constants {
    public static final String ACC_VERI_URL="/v1/verify/account";
    public static final String init="INIT";
    public static final String inProgress="IN_PROGRESS";
    public static final String success="SUCCESS";
    public static final String failure="FAILURE";

}
