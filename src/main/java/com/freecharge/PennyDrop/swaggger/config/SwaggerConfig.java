package com.freecharge.PennyDrop.swaggger.config;

import com.google.common.base.Predicates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.endpoint.mvc.EndpointHandlerMapping;
import org.springframework.boot.actuate.endpoint.mvc.MvcEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Set;


@Configuration
@EnableSwagger2
public class SwaggerConfig {
	Logger log = LoggerFactory.getLogger(SwaggerConfig.class);
	/**
	 * Posts api.
	 *
	 * @return the docket
	 */
	@Bean
	public Docket swaggerSpringMvcPlugin(final EndpointHandlerMapping actuatorEndpointHandlerMapping) {
	    ApiSelectorBuilder builder = new Docket(DocumentationType.SWAGGER_2)
	    		.groupName("seen-apis-server")
	            .useDefaultResponseMessages(false)
	            .apiInfo(apiInfo())
	            .select();

	    // Ignore the spring-boot-actuator endpoints:
	    Set<MvcEndpoint> endpoints = actuatorEndpointHandlerMapping.getEndpoints();
	    endpoints.forEach(endpoint -> {
	        String path = endpoint.getPath();
	        log.info("excluded path for swagger {}", path);
	        builder.paths(Predicates.not(PathSelectors.regex(path + ".*")));
	        builder.paths(Predicates.not(PathSelectors.regex("/error")));
	    });

	    return builder.build();
	}

	/**
	 * Api info.
	 *
	 * @return the api info
	 */
	@SuppressWarnings("deprecation")
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Seen CSA Event Alarm Tool")
				.description("EventAlarmEngine")
				.termsOfServiceUrl("See more at")
				.contact("Seen CSA Event Alarm Tools Team").license("https://www.forge.orange-labs.fr/projects/seencsatools")
				.licenseUrl("https://www.forge.orange-labs.fr/projects/seencsatools").version("1.0").build();
		
	}

}
