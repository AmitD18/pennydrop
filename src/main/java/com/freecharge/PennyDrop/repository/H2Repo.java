package com.freecharge.PennyDrop.repository;

import com.freecharge.PennyDrop.model.entity.AccountVerificationEntity;
import org.springframework.data.repository.CrudRepository;

public interface H2Repo extends CrudRepository<AccountVerificationEntity,Integer> {
}
