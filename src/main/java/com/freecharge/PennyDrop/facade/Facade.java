package com.freecharge.PennyDrop.facade;

import com.freecharge.PennyDrop.model.AcctVerifyRequest;
import com.freecharge.PennyDrop.model.entity.AccountVerificationEntity;
import com.freecharge.PennyDrop.repository.H2Repo;
import com.freecharge.PennyDrop.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Component
public class Facade {
    @Autowired
    H2Repo db;
    @Resource(name = "secureRestTemplate")
    private RestTemplate restTemplate;

    public String verifyPennyDrop(AcctVerifyRequest request) {
        Integer id = Integer.parseInt(request.getAccountNumber() + request.getIfscCode());
        if (db.findById(id).isPresent()) {
            db.getClass();
            AccountVerificationEntity data = null;
            if (data.getStatus() == Constants.success) {
                return data.getMetatData();
            }

        } else {
            //TO DO call IMPS Service
            String Response;
            AccountVerificationEntity entity = new AccountVerificationEntity();
            entity.setId(id);
            entity.setStatus(Constants.success);
            db.save(entity);
        }


        return "";
    }
}
